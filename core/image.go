// Copyright 2014 The Authors. All rights reserved.

package core

import (
	"image"
)

// ValueImage (distinguish from image.Image) with each pixel contains single value.
type ValueImage interface {
	// Bounds returns the domain for which At can return non-zero color.
	// The bounds do not necessarily contain the point (0, 0).
	Bounds() image.Rectangle
	// At returns the color of the pixel at (x, y).
	// At(Bounds().Min.X, Bounds().Min.Y) returns the upper-left pixel of the grid.
	// At(Bounds().Max.X-1, Bounds().Max.Y-1) returns the lower-right one.
	At(x, y int) float64
	// Subimage reference to main image.
	SubImage(r image.Rectangle) interface{}
	Clone() interface{} // Clone image & return reference to cloned image.
}

// ============================== Uint32Image =====================================
// Sum integral image.
type Uint32Image struct {
	// Pix holds the image's pixels. The pixel at
	// (x, y) starts at Pix[(y-Rect.Min.Y)*Stride + (x-Rect.Min.X)].
	Pix []uint32
	// Stride is the Pix stride (in bytes) between vertically adjacent pixels.
	Stride int
	// Rect is the image's bounds.
	Rect image.Rectangle
}

// NewUint32Image returns new Uint32Image image with given bounds.
func NewUint32Image(r image.Rectangle) *Uint32Image {
	w, h := r.Dx(), r.Dy()
	pix := make([]uint32, w*h)
	return &Uint32Image{pix, w, r}
}

func (p *Uint32Image) Bounds() image.Rectangle { return p.Rect }

func (p *Uint32Image) At(x, y int) float64 {
	if !(image.Point{x, y}.In(p.Rect)) {
		return float64(0)
	}
	i := p.PixOffset(x, y)
	return float64(p.Pix[i])
}

// PixOffset returns the index of the first element of Pix that corresponds to
// the pixel at (x, y).
func (p *Uint32Image) PixOffset(x, y int) int {
	return (y-p.Rect.Min.Y)*p.Stride + (x - p.Rect.Min.X)
}

func (p *Uint32Image) Set(x, y int, v uint32) {
	if !(image.Point{x, y}.In(p.Rect)) {
		return
	}
	i := p.PixOffset(x, y)
	p.Pix[i] = v
}

// SubImage returns an image representing the portion of the image p visible
// through r. The returned value shares pixels with the original image.
func (p *Uint32Image) SubImage(r image.Rectangle) interface{} {
	r = r.Intersect(p.Rect)
	// If r1 and r2 are Rectangles, r1.Intersect(r2) is not guaranteed to be inside
	// either r1 or r2 if the intersection is empty. Without explicitly checking for
	// this, the Pix[i:] expression below can panic.
	if r.Empty() {
		return &Uint32Image{}
	}
	i := p.PixOffset(r.Min.X, r.Min.Y)
	return &Uint32Image{
		Pix:    p.Pix[i:],
		Stride: p.Stride,
		Rect:   r,
	}
}

func (p *Uint32Image) Clone() interface{} {
	img := NewUint32Image(p.Rect)

	n := p.Rect.Dx()
	dy := p.Rect.Dy()
	for d0 := 0; dy > 0; dy-- {
		copy(img.Pix[d0:d0+n], p.Pix[d0:d0+n])
		d0 += img.Stride
	}
	return img
}

// ============================== Uint64Image =====================================
// Sum squared integral image.
type Uint64Image struct {
	// Pix holds the image's pixels. The pixel at
	// (x, y) starts at Pix[(y-Rect.Min.Y)*Stride + (x-Rect.Min.X)].
	Pix []uint64
	// Stride is the Pix stride (in bytes) between vertically adjacent pixels.
	Stride int
	// Rect is the image's bounds.
	Rect image.Rectangle
}

// NewUint64Image returns new Uint32Image image with given bounds.
func NewUint64Image(r image.Rectangle) *Uint64Image {
	w, h := r.Dx(), r.Dy()
	pix := make([]uint64, w*h)
	return &Uint64Image{pix, w, r}
}

func (p *Uint64Image) Bounds() image.Rectangle { return p.Rect }

func (p *Uint64Image) At(x, y int) float64 {
	if !(image.Point{x, y}.In(p.Rect)) {
		return float64(0)
	}
	i := p.PixOffset(x, y)
	return float64(p.Pix[i])
}

// PixOffset returns the index of the first element of Pix that corresponds to
// the pixel at (x, y).
func (p *Uint64Image) PixOffset(x, y int) int {
	return (y-p.Rect.Min.Y)*p.Stride + (x - p.Rect.Min.X)
}

func (p *Uint64Image) Set(x, y int, v uint64) {
	if !(image.Point{x, y}.In(p.Rect)) {
		return
	}
	i := p.PixOffset(x, y)
	p.Pix[i] = v
}

// SubImage returns an image representing the portion of the image p visible
// through r. The returned value shares pixels with the original image.
func (p *Uint64Image) SubImage(r image.Rectangle) interface{} {
	r = r.Intersect(p.Rect)
	// If r1 and r2 are Rectangles, r1.Intersect(r2) is not guaranteed to be inside
	// either r1 or r2 if the intersection is empty. Without explicitly checking for
	// this, the Pix[i:] expression below can panic.
	if r.Empty() {
		return &Uint64Image{}
	}
	i := p.PixOffset(r.Min.X, r.Min.Y)
	return &Uint64Image{
		Pix:    p.Pix[i:],
		Stride: p.Stride,
		Rect:   r,
	}
}

func (p *Uint64Image) Clone() interface{} {
	img := NewUint64Image(p.Rect)

	n := p.Rect.Dx()
	dy := p.Rect.Dy()
	for d0 := 0; dy > 0; dy-- {
		copy(img.Pix[d0:d0+n], p.Pix[d0:d0+n])
		d0 += img.Stride
	}
	return img
}

// ============================== Float64Image ====================================
// Sum squared integral image.
type Float64Image struct {
	// Pix holds the image's pixels. The pixel at
	// (x, y) starts at Pix[(y-Rect.Min.Y)*Stride + (x-Rect.Min.X)].
	Pix []float64
	// Stride is the Pix stride (in bytes) between vertically adjacent pixels.
	Stride int
	// Rect is the image's bounds.
	Rect image.Rectangle
}

// NewUint64Image returns new Float64Image image with given bounds.
func NewFloat64Image(r image.Rectangle) *Float64Image {
	w, h := r.Dx(), r.Dy()
	pix := make([]float64, w*h)
	return &Float64Image{pix, w, r}
}

func (p *Float64Image) Bounds() image.Rectangle { return p.Rect }

func (p *Float64Image) At(x, y int) float64 {
	if !(image.Point{x, y}.In(p.Rect)) {
		return float64(0)
	}
	i := p.PixOffset(x, y)
	return p.Pix[i]
}

// PixOffset returns the index of the first element of Pix that corresponds to
// the pixel at (x, y).
func (p *Float64Image) PixOffset(x, y int) int {
	return (y-p.Rect.Min.Y)*p.Stride + (x - p.Rect.Min.X)
}

func (p *Float64Image) Set(x, y int, v float64) {
	if !(image.Point{x, y}.In(p.Rect)) {
		return
	}
	i := p.PixOffset(x, y)
	p.Pix[i] = v
}

// SubImage returns an image representing the portion of the image p visible
// through r. The returned value shares pixels with the original image.
func (p *Float64Image) SubImage(r image.Rectangle) interface{} {
	r = r.Intersect(p.Rect)
	// If r1 and r2 are Rectangles, r1.Intersect(r2) is not guaranteed to be inside
	// either r1 or r2 if the intersection is empty. Without explicitly checking for
	// this, the Pix[i:] expression below can panic.
	if r.Empty() {
		return &Float64Image{}
	}
	i := p.PixOffset(r.Min.X, r.Min.Y)
	return &Float64Image{
		Pix:    p.Pix[i:],
		Stride: p.Stride,
		Rect:   r,
	}
}

func (p *Float64Image) Clone() interface{} {
	img := NewFloat64Image(p.Rect)

	n := p.Rect.Dx()
	dy := p.Rect.Dy()
	for d0 := 0; dy > 0; dy-- {
		copy(img.Pix[d0:d0+n], p.Pix[d0:d0+n])
		d0 += img.Stride
	}
	return img
}
