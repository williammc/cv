// Copyright 2014 The Authors. All rights reserved.

package core

import (
	"image"
	"image/color"
)

type ImageAccessError struct {
	Message string
}

func (e ImageAccessError) Error() string {
	return e.Message
}

// Efficient image accessor interface for image.Image
type ImageAccessor interface {
	// Go image package interface
	image.Image
	Set(x, y int, c color.Color) // Set color at (x, y).

	// Additional interface
	DataAt(x, y int) interface{}              // Generic access to data at specific pixel location.
	PerChannelDataAt(x, y, i int) interface{} // Pointer to value at location (x, y) and channel i.

	// Convenient methods.
	SetZeroAt(x, y int) error // Set zero values at pixel(x, y).
	Channels() int            // Number of color channels per pixel.
	Clone() image.Image       // Clone image & return reference to cloned image.

}

// RGBA Accesssor =============================================================
type RGBAAcessor struct {
	*image.RGBA
}

func NewRGBAAccessor(p *image.RGBA) *RGBAAcessor {
	return &RGBAAcessor{p}
}

func (p *RGBAAcessor) DataAt(x, y int) interface{} {
	if !(image.Point{x, y}.In(p.Rect)) {
		return []uint8{}
	}
	id := p.PixOffset(x, y)
	return p.Pix[id : id+3]
}

func (p *RGBAAcessor) Pixels() []uint8 {
	return p.Pix
}

// Access pixel
func (p *RGBAAcessor) PerChannelDataAt(x, y, i int) interface{} {
	if !(image.Point{x, y}.In(p.Rect)) || i >= 4 {
		return nil
	}
	id := p.PixOffset(x, y)
	return &p.Pix[id+i]
}

// Set all pixels at (x,y) to Zeros
func (p *RGBAAcessor) SetZeroAt(x, y int) error {
	if !(image.Point{x, y}.In(p.Rect)) {
		return ImageAccessError{"Out of bound"}
	}
	id := p.PixOffset(x, y)
	p.Pix[id] = 0
	p.Pix[id+1] = 0
	p.Pix[id+2] = 0
	p.Pix[id+3] = 0
	return nil
}

func (p *RGBAAcessor) Channels() int {
	return 4
}

func (p *RGBAAcessor) Clone() image.Image {
	img := image.NewRGBA(p.Bounds())

	n := 4 * p.Rect.Dx()
	dy := 4 * p.Rect.Dy()
	for d0 := 0; dy > 0; dy-- {
		copy(img.Pix[d0:d0+n], p.Pix[d0:d0+n])
		d0 += img.Stride
	}
	return img
}

// Gray Accesssor =============================================================
type GrayAcessor struct {
	*image.Gray
}

func NewGrayAccessor(p *image.Gray) *GrayAcessor {
	return &GrayAcessor{p}
}

func (p *GrayAcessor) DataAt(x, y int) interface{} {
	if !(image.Point{x, y}.In(p.Rect)) {
		return nil
	}
	id := p.PixOffset(x, y)
	return &p.Pix[id]
}

func (p *GrayAcessor) Pixels() []uint8 {
	return p.Pix
}

// Access pixel
func (p *GrayAcessor) PerChannelDataAt(x, y, i int) interface{} {
	if !(image.Point{x, y}.In(p.Rect)) || i != 0 {
		return nil
	}
	id := p.PixOffset(x, y)
	return &p.Pix[id]
}

// Set all pixels at (x,y) to Zeros
func (p *GrayAcessor) SetZeroAt(x, y int) error {
	if !(image.Point{x, y}.In(p.Rect)) {
		return ImageAccessError{"Out of bound"}
	}
	id := p.PixOffset(x, y)
	p.Pix[id] = 0
	return nil
}

func (p *GrayAcessor) Channels() int {
	return 1
}

func (p *GrayAcessor) Clone() image.Image {
	img := image.NewGray(p.Bounds())
	n := p.Rect.Dx()
	dy := p.Rect.Dy()
	for d0 := 0; dy > 0; dy-- {
		copy(img.Pix[d0:d0+n], p.Pix[d0:d0+n])
		d0 += img.Stride
	}
	return img
}

// Get image width, height
func getImageWidthHeight(p ImageAccessor) (width, height int) {
	width = p.(image.Image).Bounds().Max.X - p.(image.Image).Bounds().Min.X
	height = p.(image.Image).Bounds().Max.Y - p.(image.Image).Bounds().Min.Y
	return
}
