// Copyright 2014 The Authors. All rights reserved.

package util

import (
	"bufio"
	"image"
	"image/jpeg"
	"image/png"
	"log"
	"os"
	"runtime"
)

// Set number of parallel jobs.
// if #CPUs > width
func GetNumJobs(d int) (n int) {
	n = runtime.NumCPU()
	if n > d {
		n = d
	}
	return
}

// Save image to file_path as png format.
func SaveToFile(file_path string, m image.Image) bool {
	f, err := os.Create(file_path)
	if err != nil {
		log.Println(err)
		return false
	}
	defer f.Close()
	b := bufio.NewWriter(f)
	switch file_path[len(file_path)-3:] {
	case "png":
		err = png.Encode(b, m)
	case "jpg":
		err = jpeg.Encode(b, m, nil)
	default:
		log.Println("Image type not supported!")
		return false
	}
	if err != nil {
		log.Println(err)
		return false
	}
	err = b.Flush()
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}
