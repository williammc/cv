// Copyright 2014 The Authors. All rights reserved.

package feature2d

import (
	"bitbucket.org/williammccohen/cv/geometry"
	"image"
)

type KeyPoint struct {
	Location geometry.Vector2d
	// Feature size
	Size float64
	// feature orintation in degrees
	// (has negative value if the orientation
	// is not defined/not computed)
	Angle float64
	// feature strength
	// (can be used to select only
	// the most prominent key points)
	Response float64
	// scale-space octave in which the feature has been found;
	// may correlate with the size
	octave int
}

func NewKeyPoint() *KeyPoint {
	kp := new(KeyPoint)
	kp.Location = *geometry.MakeVector2d(0, 0)
	return kp
}

type Descriptor interface {
	Distance(d *Descriptor) float64
}

// 2D feature detection.
type FeatureDetector interface {
	Detect(image.Image) []KeyPoint
}

// 2D feature extraction for descriptor.
type DescriptorExtractor interface {
	Extract(image.Image, []KeyPoint) []Descriptor
}

// ========================== Normal Descriptor: SURF, SIFT =======================
type NormalDescriptor struct {
	data []float64
	size uint
}

func NewNormalDescriptor(size uint) *NormalDescriptor {
	p := new(NormalDescriptor)
	p.size = size
	p.data = make([]float64, size, size)
	return p
}

// ========================== Normal Descriptor: BRIEF, ORB =======================
type BinaryDescriptor struct {
	data []byte
	size uint
}

func NewBinaryDescriptor(size uint) *BinaryDescriptor {
	p := new(BinaryDescriptor)
	p.size = size
	p.data = make([]byte, size, size)
	return p
}

// Calculate distance between two descriptor.
func (bd *BinaryDescriptor) Distance(a_bd *BinaryDescriptor) (d float64) {

	return
}
