// Copyright 2014 The Authors. All rights reserved.

package feature2d

import (
	"image"
)

// 2D feature detection.
type OrbFeatureDetector struct {
	number_features     uint    // Limiting number of features
	intensity_threshold uint    // Intensity threshold (typically 20).
	octaves             uint    // Number of levels in pyramid image.
	octave_ratio        float64 // Scale down ratio in pyramid image.
}

func NewOrbFeatureDetector(nf, threshold, octaves uint, ratio float64) *OrbFeatureDetector {
	of := new(OrbFeatureDetector)
	of.number_features = nf
	of.intensity_threshold = threshold
	of.octaves = octaves
	of.octave_ratio = ratio
	return of
}

// Perform ORB features detection.
func (od *OrbFeatureDetector) Detect(*image.Gray) (keypoints []KeyPoint) {

	//TODO: loop through each pyramid level, detec FAST
	return
}

// ORB descriptor extractor.
type OrbDescriptorExtractor struct {
}

func (od *OrbDescriptorExtractor) Extract(*image.Gray, []KeyPoint) (descriptors []Descriptor) {
	//TODO: for each keypoint, compute orientation, then descriptor.
	return
}
