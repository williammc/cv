// Copyright 2014 The Authors. All rights reserved.

package feature2d

import (
	"bitbucket.org/williammccohen/cv/geometry"
	"code.google.com/p/draw2d/draw2d"
	"image"
	"image/color"
)

// Draw keypoints.
func DrawKeypoints(img *image.RGBA, keypoints []KeyPoint, col color.Color, radius float64) {
	gc := draw2d.NewGraphicContext(img)
	for _, kp := range keypoints {
		DrawCross(gc, kp.Location, col, radius)
	}
}

// Draw cross.
func DrawCross(gc draw2d.GraphicContext, loc geometry.Vector2d,
	col color.Color, radius float64) {
	gc.SetStrokeColor(col)
	gc.MoveTo(loc.Get(0), loc.Get(1))
	gc.LineTo(loc.Get(0)-radius, loc.Get(1))
	gc.Stroke()
	gc.MoveTo(loc.Get(0), loc.Get(1))
	gc.LineTo(loc.Get(0)+radius, loc.Get(1))
	gc.Stroke()
	gc.MoveTo(loc.Get(0), loc.Get(1))
	gc.LineTo(loc.Get(0), loc.Get(1)-radius)
	gc.Stroke()
	gc.MoveTo(loc.Get(0), loc.Get(1))
	gc.LineTo(loc.Get(0), loc.Get(1)+radius)
	gc.Stroke()
}
