// Copyright 2014 The Authors. All rights reserved.

package feature2d

import (
	"bitbucket.org/williammccohen/cv/imgproc"
	"bitbucket.org/williammccohen/cv/util"
	"image"
	"math"
)

// 2D feature detection.
type FastFeatureDetector struct {
	number_features     uint  // Limited number of features.
	intensity_threshold uint8 // Intesity difference threshold.

}

func NewFastFetureDetector(number_features uint, intensity_threshold uint8) (fd *FastFeatureDetector) {
	fd = new(FastFeatureDetector)
	fd.number_features = number_features
	fd.intensity_threshold = intensity_threshold
	return
}

// Perform FAST (alike) features detection.
func (fd *FastFeatureDetector) Detect(img *image.Gray, nonmaxima_suppression bool) []KeyPoint {
	img_a := imgproc.NewGrayAccessor(img)
	score_img := imgproc.NewFloat64Image(img.Rect)

	b := img.Bounds()
	n := util.GetNumJobs(b.Dy())
	kp_channel := make(chan KeyPoint, fd.number_features)
	done := make(chan bool, n)
	stop := false
	for i := 0; i < n; i++ {
		min_x := b.Min.X + 1
		max_x := b.Max.X - 1
		min_y := b.Min.Y + i*(b.Dy())/n
		if i == 0 {
			min_y = b.Min.Y + 1
		}
		max_y := b.Min.Y + (i+1)*(b.Dy())/n
		if i == n-1 {
			max_y = b.Min.Y - 1 + (i+1)*(b.Dy())/n
		}
		go func(b image.Rectangle, kp_channel chan KeyPoint, done chan bool) {
			var diff_x1, diff_x2, diff_y1, diff_y2 uint8
			for y := b.Min.Y; y < b.Max.Y; y++ {
				for x := b.Min.X; x < b.Max.X; x++ {
					if stop {
						done <- true
						return
					}
					diff_x1 = calc_minus_abs(*img_a.PerChannelDataAt(x-1, y, 0).(*uint8), *img_a.PerChannelDataAt(x+1, y, 0).(*uint8))
					diff_y1 = calc_minus_abs(*img_a.PerChannelDataAt(x, y-1, 0).(*uint8), *img_a.PerChannelDataAt(x, y+1, 0).(*uint8))
					if diff_y1 >= fd.intensity_threshold && diff_x1 >= fd.intensity_threshold {
						diff_y2 = calc_minus_abs(*img_a.PerChannelDataAt(x-1, y-1, 0).(*uint8), *img_a.PerChannelDataAt(x+1, y+1, 0).(*uint8))
						diff_x2 = calc_minus_abs(*img_a.PerChannelDataAt(x-1, y+1, 0).(*uint8), *img_a.PerChannelDataAt(x+1, y-1, 0).(*uint8))

						if diff_y2 >= fd.intensity_threshold && diff_x2 >= fd.intensity_threshold {
							keypoint := NewKeyPoint()
							keypoint.Response = float64(diff_x1) + float64(diff_y1) +
								float64(diff_x2) + float64(diff_y2)
							keypoint.Location.Set(0, float64(x))
							keypoint.Location.Set(1, float64(y))
							score_img.Set(x, y, keypoint.Response)
							kp_channel <- *keypoint
						}
					}
				}
			}
			done <- true
		}(image.Rect(min_x, min_y, max_x, max_y), kp_channel, done)
	}

	keypoints := make([]KeyPoint, 0, fd.number_features)
	for !stop {
		for kp := range kp_channel {
			keypoints = append(keypoints, kp)
			if uint(len(keypoints)) >= fd.number_features {
				stop = true
				break
			}
		}
	}

	for i := 0; i < n; i++ {
		<-done
	}

	// non-maxima suppression
	if nonmaxima_suppression {
		// Figure out hash tabel cell size given number of features.
		// 2 hash tables (diff cell size) are for avoiding close features in cell border.
		cell_size_step2 := uint(math.Sqrt(float64(b.Dx()*b.Dy()) / 500))
		cell_size_step1 := cell_size_step2/2 + 1

		grid_width_step1, hash_step_1 := make_hash_table(b, cell_size_step1)
		for i, kp := range keypoints {
			index := hash_location(uint(kp.Location.Get(0)), uint(kp.Location.Get(1)),
				cell_size_step1, grid_width_step1)
			hash_step_1[index] = append(hash_step_1[index], uint(i))
		}
		grid_width_step_2, hash_step_2 := make_hash_table(b, cell_size_step2)
		var max_score float64
		var max_id, index uint
		for i := 0; i < len(hash_step_1); i++ {
			if len(hash_step_1[i]) > 0 {
				max_score, max_id = 0.0, 0

				for j := 0; j < len(hash_step_1[i]); j++ {
					if max_score < keypoints[hash_step_1[i][j]].Response {
						max_score = keypoints[hash_step_1[i][j]].Response
						max_id = hash_step_1[i][j]
					}
				}
				index = hash_location(uint(keypoints[max_id].Location.Get(0)),
					uint(keypoints[max_id].Location.Get(1)), cell_size_step2, grid_width_step_2)
				hash_step_2[index] = append(hash_step_2[index], max_id)
			}
		}

		nonmaxima_keypoints := make([]KeyPoint, 0, fd.number_features)
		for i := 0; i < len(hash_step_2); i++ {
			if len(hash_step_2[i]) > 0 {
				max_score, max_id = 0.0, 0
				for j := 0; j < len(hash_step_2[i]); j++ {
					if max_score < keypoints[hash_step_2[i][j]].Response {
						max_score = keypoints[hash_step_2[i][j]].Response
						max_id = hash_step_2[i][j]
					}
				}
				nonmaxima_keypoints = append(nonmaxima_keypoints, keypoints[max_id])
			}
		}
		return nonmaxima_keypoints
	}
	return keypoints
}

// Calculate absolute value of (a-b)
func calc_minus_abs(a, b uint8) uint8 {
	if a > b {
		return a - b
	} else {
		return b - a
	}
	return 0
}

// Make hash table.
func make_hash_table(b image.Rectangle, grid_cell_size uint) (uint, [][]uint) {
	grid_width := b.Dx()/6 + 1
	if b.Dx()%6 == 0 {
		grid_width = b.Dx() / 6
	}
	grid_height := b.Dy()/6 + 1
	if b.Dy()%6 == 0 {
		grid_height = b.Dy() / 6
	}
	hash_table := make([][]uint, grid_width*grid_height)
	return uint(grid_width), hash_table
}

// Hash a pixel location (x, y) to index of hash-table.
func hash_location(x, y, grid_cell_size, grid_width uint) uint {
	x = x / grid_cell_size
	y = y / grid_cell_size
	index := y*grid_width + x
	return index
}
