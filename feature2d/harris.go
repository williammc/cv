// Copyright 2014 The Authors. All rights reserved.

package feature2d

import (
	"bitbucket.org/williammccohen/cv/imgproc"
	"image"
)

// Calculate harris-response on given image.
func HarrisResponse(img *image.Gray) float64 {
	gx, gy := imgproc.CalculateGradient(img)
	a, b, c := 0.0, 0.0, 0.0
	bound := img.Bounds()
	for y := bound.Min.Y; y < bound.Max.Y; y++ {
		for x := bound.Min.X; x < bound.Max.X; x++ {
			a += gx.At(x, y) * gx.At(x, y)
			b += gx.At(x, y) * gy.At(x, y)
			c += gy.At(x, y) * gy.At(x, y)
		}
	}
	return (a*b - c*c) - 0.04*((a+b)*(a+b))
}
