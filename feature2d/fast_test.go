// Copyright 2014 The Authors. All rights reserved.

package feature2d

import (
	"bitbucket.org/williammccohen/cv/imgproc"
	"bitbucket.org/williammccohen/cv/util"
	"image"
	"image/color"
	"image/jpeg"
	"log"
	"os"
	"runtime"
	"testing"
)

var (
	img       image.Image
	rgba_img  *image.RGBA
	gray_img  *image.Gray
	fd        *FastFeatureDetector
	data_path string
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	data_path, _ = os.Getwd()
	data_path = data_path + "/../data/"
	// open image
	file, err := os.Open(data_path + "family.jpg")
	if err != nil {
		log.Fatal(err)
	}

	// decode jpeg into image.Image
	img, err = jpeg.Decode(file)
	if err != nil {
		log.Fatal(err)
	}
	file.Close()
	gray_img = imgproc.ConvertImageToGrayscale(img)

	fd = NewFastFetureDetector(5000, 30)
}

// Test FAST features detection.
func TestFast(t *testing.T) {
	// detect
	keypoints := fd.Detect(gray_img, true)

	// draw detected points
	rgba_img = imgproc.ConvertImageToRGBA(img)
	DrawKeypoints(rgba_img, keypoints, color.RGBA{255, 0, 0, 255}, 5)

	file_path := data_path + "family_fast.jpg"
	util.SaveToFile(file_path, rgba_img)
}

// Benchmark for FAST features detection of 640x480 grayscale image.
func BenchmarkFast640x480(b *testing.B) {
	// detect
	fd := NewFastFetureDetector(5000, 30)
	_ = fd.Detect(gray_img, true)
}
