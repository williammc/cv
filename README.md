Computer Vision library
Version 1.0


# About CV
`CV` is a computer vision library focusing on real-time functionalities and performance. The library will be purely using golang for implementation.

Monday 31st March 2014
William McCohen

# Todo
1. Draw plan for a core computer vision data structures (consulting opencv) & features.
2. Redesign ValueImage (employing color.Model from go.image package).
	- only support single channel value-based images: Float32Image, Float64Image
3. Improve image accessors (efficient, general to color-based go.image.Image & value-based cv.image.Image).
4. Refine geometry component 
	- point: Point2D, Point3D, Point4D (derived from Vector)
	- matrix: Matrix2D, Matrix3D, Matrix4D (derived from go.matrix.DenseMatrix)
5. Improve documentation.
6. plan for releases.



# Supporting functionalities
## Core structures
- images, points, matrixes.
- drawing on images.

## Image processing
- integral, gradient, sobel, interpolation, resize, transform.

## Feature2d
- essential image 2D features: FAST, ORB, BRISK, FAI, SURF.

## Camera models
- linear camera, polinomial camera.
- calibration support.

# DEPENDENCIES
- github.com/skelterjohn/go.matrix
- code.google.com/p/draw2d/draw2d
