// Copyright 2014 The Authors. All rights reserved.

package imgproc

import (
	"bitbucket.org/williammccohen/cv/util"
	"image"
	"image/color"
	"log"
)

func replicate_border(x, y int, rect image.Rectangle) (xx, yy int) {
	f := func(x, min, max int) int {
		if x < min {
			x = min
		} else if x >= max {
			x = max - 1
		}

		return x
	}
	xx = f(x, rect.Min.X, rect.Max.X)
	yy = f(y, rect.Min.Y, rect.Max.Y)
	return
}

// Trans2 is a 2-dimensional linear transformation.
type Trans2 [6]float32

// Apply the transformation to a point (x,y).
func (t *Trans2) Eval(x, y float32) (u, v float32) {
	u = t[0]*x + t[1]*y + t[2]
	v = t[3]*x + t[4]*y + t[5]
	return
}

// InterpolationFunction return a Filter implementation
// that operates on an image. Two factors
// allow to scale the filter kernels in x- and y-direction
// to prevent moire patterns.
type InterpolationFunction func(ImageAccessor, [2]float32) *SamplingFilter

// Resize an image to new width and height using the interpolation function interp.
// A new image with the given dimensions will be returned.
// If one of the parameters width or height is set to 0, its size will be calculated so that
// the aspect ratio is that of the originating image.
// The resizing algorithm uses channels for parallel computation.
func Resize(width, height uint, img image.Image, interp InterpolationFunction) image.Image {
	oldBounds := img.Bounds()
	old_width := float32(oldBounds.Dx())
	old_height := float32(oldBounds.Dy())

	scale_x, scale_y := calc_factors(width, height, old_width, old_height)
	t := Trans2{scale_x, 0, float32(oldBounds.Min.X), 0, scale_y, float32(oldBounds.Min.Y)}

	var a_img ImageAccessor
	var resized_img image.Image
	var a_resized_img ImageAccessor
	switch img.(type) {
	case *image.Gray:
		a_img = NewGrayAccessor(img.(*image.Gray))
		resized_img = image.NewGray(image.Rect(0, 0, int(old_width/scale_x), int(old_height/scale_y)))
		a_resized_img = NewGrayAccessor(resized_img.(*image.Gray))
		break

	case *image.RGBA:
		a_img = NewRGBAAccessor(img.(*image.RGBA))
		resized_img = image.NewRGBA(image.Rect(0, 0, int(old_width/scale_x), int(old_height/scale_y)))
		a_resized_img = NewRGBAAccessor(resized_img.(*image.RGBA))
		break
	default:
		log.Fatal("Unsupported image type.")
	}

	b := resized_img.Bounds()

	n := util.GetNumJobs(b.Dy())
	c := make(chan int, n)
	var xx, yy int
	for i := 0; i < n; i++ {
		go func(b image.Rectangle, c chan int) {
			filter := interp(a_img, [2]float32{clamp_factor(scale_x), clamp_factor(scale_y)})
			var u, v float32
			var col color.Color
			for y := b.Min.Y; y < b.Max.Y; y++ {
				for x := b.Min.X; x < b.Max.X; x++ {
					u, v = t.Eval(float32(x), float32(y))
					col = filter.Interpolate(u, v)
					xx, yy = replicate_border(x, y, a_resized_img.Bounds())
					a_resized_img.Set(xx, yy, col)
				}
			}
			c <- 1
		}(image.Rect(b.Min.X, b.Min.Y+i*(b.Dy())/n, b.Max.X, b.Min.Y+(i+1)*(b.Dy())/n), c)
	}

	for i := 0; i < n; i++ {
		<-c
	}

	return resized_img
}

// Calculate scaling factors using old and new image dimensions.
func calc_factors(width, height uint, old_width, old_height float32) (scale_x, scale_y float32) {
	if width == 0 {
		if height == 0 {
			scale_x = 1.0
			scale_y = 1.0
		} else {
			scale_y = old_height / float32(height)
			scale_x = scale_y
		}
	} else {
		scale_x = old_width / float32(width)
		if height == 0 {
			scale_y = scale_x
		} else {
			scale_y = old_height / float32(height)
		}
	}
	return
}

// Set filter scaling factor to avoid moire patterns.
// This is only useful in case of downscaling (factor>1).
func clamp_factor(factor float32) float32 {
	if factor < 1 {
		factor = 1
	}
	return factor
}

func NearestNeighbor(a_img ImageAccessor, factor [2]float32) *SamplingFilter {
	sf := SamplingFilter{NewNearestNeighborKernel(), a_img, factor}
	return &sf
}

func Bilinear(a_img ImageAccessor, factor [2]float32) *SamplingFilter {
	sf := SamplingFilter{NewBilinearKernel(), a_img, factor}
	return &sf
}

func BiCubic(a_img ImageAccessor, factor [2]float32) *SamplingFilter {
	sf := SamplingFilter{NewBiCubicKernel(), a_img, factor}
	return &sf
}
