// Copyright 2014 The Authors. All rights reserved.

package imgproc

import (
	"bitbucket.org/williammccohen/cv/util"
	"image"
)

func CalculateGradient(img *image.Gray) (gx, gy *Float64Image) {
	gx = NewFloat64Image(img.Rect)
	gy = NewFloat64Image(img.Rect)
	img_a := NewGrayAccessor(img)

	b := img.Bounds()
	n := util.GetNumJobs(b.Dy())
	done := make(chan bool, n)
	for i := 0; i < n; i++ {
		min_x := b.Min.X + 1
		max_x := b.Max.X - 1
		min_y := b.Min.Y + i*(b.Dy())/n
		if i == 0 {
			min_y = b.Min.Y + 1
		}
		max_y := b.Min.Y + (i+1)*(b.Dy())/n
		if i == n-1 {
			max_y = b.Min.Y - 1 + (i+1)*(b.Dy())/n
		}
		go func(b image.Rectangle, done chan bool) {
			var diff_x, diff_y float64
			for y := b.Min.Y; y < b.Max.Y; y++ {
				for x := b.Min.X; x < b.Max.X; x++ {

					diff_x = float64(*img_a.DataAt(x+1, y).(*uint8)) -
						float64(*img_a.DataAt(x, y).(*uint8))
					gx.Set(x, y, diff_x)

					diff_y = float64(*img_a.DataAt(x, y+1).(*uint8)) -
						float64(*img_a.DataAt(x, y).(*uint8))
					gy.Set(x, y, diff_y)
				}
			}
			done <- true
		}(image.Rect(min_x, min_y, max_x, max_y), done)
	}

	for i := 0; i < n; i++ {
		<-done
	}
	return
}
