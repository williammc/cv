// Copyright 2014 The Authors. All rights reserved.

package imgproc

import (
	"bitbucket.org/williammccohen/gocv/util"
	"image"
	"image/jpeg"
	"log"
	"os"
	"runtime"
	"testing"
)

var (
	g_img      image.Image
	g_rgba_img *image.RGBA
	g_gray_img *image.Gray
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	// open image
	file, err := os.Open("/home/thanh/data/goar/family.jpg")
	if err != nil {
		log.Fatal(err)
	}

	// decode jpeg into image.Image
	g_img, err = jpeg.Decode(file)
	if err != nil {
		log.Fatal(err)
	}
	file.Close()
	g_gray_img = ConvertImageToGrayscale(g_img)
}

// Test gradient calculation.
func TestGradient(t *testing.T) {
	gx, gy := CalculateGradient(g_gray_img)

	file_path := "/home/thanh/data/goar/family_gx.jpg"
	gray_gx := ConvertValueImageToGrayscale(gx)
	util.SaveToFile(file_path, gray_gx)
	file_path = "/home/thanh/data/goar/family_gy.jpg"
	gray_gy := ConvertValueImageToGrayscale(gy)
	util.SaveToFile(file_path, gray_gy)
}

// Benchmark for gradient calculation on 640x480 grayscale image.
func BenchmarkGradient640x480(b *testing.B) {
	_, _ = CalculateGradient(g_gray_img)
}
