// Copyright 2014 The Authors. All rights reserved.

package imgproc

import (
	"image/color"
	"log"
	"math"
)

// SamplingFilter can interpolate at points (x,y)
type SamplingFilter struct {
	kernel Kernel        // Sampling kernel.
	img    ImageAccessor // Input image.
	factor [2]float32
}

func (sf *SamplingFilter) Interpolate(x, y float32) (col color.Color) {
	xf := uint(math.Floor(float64(x)))
	yf := uint(math.Floor(float64(y)))
	tx := x - float32(xf)
	ty := y - float32(yf)
	var c []float32
	switch sf.img.(type) {
	case *GrayAcessor:
		c = append(c, 0.0)
		break
	case *RGBAAcessor:
		c = append(c, []float32{0, 0, 0}...)
		break
	default:
		log.Fatal("Image type not supported.")
	}

	sum := float32(0)
	b := sf.kernel.Bounds()
	var v []uint8
	var ker float32
	var xx, yy int
	for j := b.Min.Y; j <= b.Max.Y; j++ {
		for i := b.Min.X; i <= b.Max.X; i++ {
			ker = sf.kernel.Eval(tx-float32(i), ty-float32(j))
			xx, yy = replicate_border(int(xf)+i, int(yf)+j, sf.img.Bounds())
			v = sf.img.DataAt(xx, yy).([]uint8)
			for k := range c {
				c[k] += float32(v[k]) * ker
			}
			sum += ker
		}
	}
	// normalize
	for k := range c {
		c[k] /= sum
	}
	switch sf.img.(type) {
	case *GrayAcessor:
		col = color.Gray{uint8(c[0])}
		break
	case *RGBAAcessor:
		col = color.RGBA{uint8(c[0]), uint8(c[1]), uint8(c[2]), uint8(c[3])}
		break
	}
	return
}
