// Copyright 2014 The Authors. All rights reserved.

// Transform 2D image
package imgproc

import (
	matrix "github.com/skelterjohn/go.matrix"
	"image"
	"image/color"
)

// Transform 2D image from in_image to out_image given 2x2 transformation
// matrix from Out to In (inverse) and origins of the transformation.
// Return number of pixels outside in_image
func Transform(in_image_accessor ImageAccessor, out_image_accessor ImageAccessor,
	M *matrix.DenseMatrix,
	in_orig *matrix.DenseMatrix, out_orig *matrix.DenseMatrix) (count int) {

	iw, ih := getImageWidthHeight(in_image_accessor)
	ow, oh := getImageWidthHeight(out_image_accessor)

	across := M.GetColVector(0)
	down := M.GetColVector(1)

	temp, _ := M.Times(out_orig)
	p0, _ := in_orig.Minus(temp)
	//p1, _ := p0.Plus(matrix.Scaled(across, float64(ow)))
	//p2, _ := p0.Plus(matrix.Scaled(down, float64(oh)))
	p3, _ := p0.Plus(matrix.Scaled(across, float64(ow)))
	p3, _ = p3.Plus(matrix.Scaled(down, float64(oh)))

	// ul --> p0
	// ur --> w*across + p0
	// ll --> h*down + p0
	// lr --> w*across + h*down + p0
	min_x, min_y := p0.Get(0, 0), p0.Get(1, 0)
	max_x, max_y := min_x, min_y

	// Determin bounds
	if across.Get(0, 0) < 0 {
		min_x += float64(ow) * across.Get(0, 0)
	} else {
		max_x += float64(ow) * across.Get(0, 0)
	}
	if down.Get(0, 0) < 0 {
		min_x += float64(oh) * down.Get(0, 0)
	} else {
		max_x += float64(oh) * down.Get(0, 0)
	}
	if across.Get(1, 0) < 0 {
		min_y += float64(ow) * across.Get(1, 0)
	} else {
		max_y += float64(ow) * across.Get(1, 0)
	}
	if down.Get(1, 0) < 0 {
		min_y += float64(oh) * down.Get(1, 0)
	} else {
		max_y += float64(oh) * down.Get(1, 0)
	}

	// This gets from the end of one row to the beginning of the next
	carriage_return, _ := down.Minus(matrix.Scaled(across, float64(ow)))

	// If the patch being extracted is completely in the image then no
	// check is needed with each point.

	var v color.Color
	count = 0
	if min_x >= 0.0 && min_y >= 0.0 && max_x < float64(iw-1) && max_y < float64(ih-1) {
		p := p0
		for i := 0; i < oh; i++ {
			_ = p.Add(carriage_return)
			for j := 0; j < ow; j++ {
				_ = p.Add(across)
				v = sample(in_image_accessor, p.Get(0, 0), p.Get(1, 0))
				out_image_accessor.Set(j, i, v)
			}
		}
	} else { // Check each source location
		// Store as doubles to avoid conversion cost for comparison
		x_bound := float64(iw - 1)
		y_bound := float64(ih - 1)
		p := p0
		for i := 0; i < ih; i++ {
			_ = p.Add(carriage_return)
			for j := 0; j < iw; j++ {
				_ = p.Add(across)
				// Make sure that we are extracting pixels in the image
				if 0 <= p.Get(0, 0) && 0 <= p.Get(1, 0) &&
					p.Get(0, 0) < x_bound && p.Get(1, 0) < y_bound {
					v = sample(in_image_accessor, p.Get(0, 0), p.Get(1, 0))
					out_image_accessor.Set(j, i, v)
				} else {
					out_image_accessor.SetZeroAt(j, i)
					count++
				}
			}
		}
	}
	return

}

// Sample one pixel color at given 2D location
func sample(img ImageAccessor, x float64, y float64) (v color.Color) {
	lx, ly := int(x), int(y)
	x -= float64(lx)
	y -= float64(ly)
	channels := 1
	switch img.(image.Image).ColorModel() {
	case color.RGBAModel:
		channels = 4
	case color.GrayModel:
		channels = 1
	}
	pixel_value := make([]uint8, channels, channels)
	for i := 0; i < channels; i++ {
		pixel_value[i] = uint8((1-y)*((1-x)*float64(*img.PerChannelDataAt(lx, ly, i).(*uint8))+
			x*float64(*img.PerChannelDataAt(lx+1, ly, i).(*uint8))) +
			y*((1-x)*float64(*img.PerChannelDataAt(lx, ly+1, i).(*uint8))+
				x*float64(*img.PerChannelDataAt(lx+1, ly+1, i).(*uint8))))
	}

	switch img.(image.Image).ColorModel() {
	case color.RGBAModel:
		v = color.NRGBA{pixel_value[0], pixel_value[1], pixel_value[2], pixel_value[3]}
	case color.GrayModel:
		channels = 3
		v = color.Gray{pixel_value[0]}
	}
	return

}
