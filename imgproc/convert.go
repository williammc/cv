// Authors: Thanh Nguyen<thanh@icg.tugraz.at>
// Copyright 2014, All rights reserved.

package imgproc

import (
	"image"
	"image/color"
)

// Convert any image to grayscale.
func ConvertImageToGrayscale(src image.Image) *image.Gray {
	// Create a new grayscale image
	b := src.Bounds()
	gray := image.NewGray(b)
	for y := b.Min.Y; y < b.Max.Y; y++ {
		for x := b.Min.X; x < b.Max.X; x++ {
			old_color := src.At(x, y)
			gray_color := color.GrayModel.Convert(old_color)
			gray.Set(x, y, gray_color)
		}
	}
	return gray
}

// Convert any image to RGBA.
func ConvertImageToRGBA(src image.Image) *image.RGBA {
	// Create a new RGBA image
	b := src.Bounds()
	rgba := image.NewRGBA(b)
	for y := b.Min.Y; y < b.Max.Y; y++ {
		for x := b.Min.X; x < b.Max.X; x++ {
			old_color := src.At(x, y)
			gray_color := color.RGBAModel.Convert(old_color)
			rgba.Set(x, y, gray_color)
		}
	}
	return rgba
}

// Convert any image to grayscale.
func ConvertValueImageToGrayscale(src ValueImage) *image.Gray {
	// Create a new grayscale image
	b := src.Bounds()
	gray := image.NewGray(b)
	for y := b.Min.Y; y < b.Max.Y; y++ {
		for x := b.Min.X; x < b.Max.X; x++ {
			old_color := src.At(x, y)
			new_color := uint8(0)
			if old_color > 255 {
				new_color = 255
			} else if old_color < 0 {
				new_color = 0
			} else {
				new_color = uint8(old_color)
			}
			gray.Set(x, y, color.Gray{new_color})
		}
	}
	return gray
}
