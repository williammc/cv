// Copyright 2014 The Authors. All rights reserved.

package imgproc

import (
	"bitbucket.org/williammccohen/cv/util"
	"image"
)

// Sobel operator given kernel size.
func Sobel(img *image.Gray, dx, dy uint) (deriv *Float64Image) {
	deriv = NewFloat64Image(img.Bounds())
	a_img := NewGrayAccessor(img)
	sobel_kernel := NewSobelKernel(dx, dy)

	b := img.Bounds()

	n := util.GetNumJobs(b.Dy())
	done := make(chan int, n)
	kernel_b := sobel_kernel.Bounds()
	for i := 0; i < n; i++ {
		go func(b image.Rectangle) {
			var xx, yy int
			var sum, ker float32
			var col *uint8
			for y := b.Min.Y; y < b.Max.Y; y++ {
				for x := b.Min.X; x < b.Max.X; x++ {
					sum = 0
					for j := kernel_b.Min.Y; j <= kernel_b.Max.Y; j++ {
						for i := kernel_b.Min.X; i <= kernel_b.Max.X; i++ {
							ker = sobel_kernel.Eval(float32(i), float32(j))
							xx, yy = replicate_border(x+i, y+j, img.Bounds())
							col = a_img.DataAt(xx, yy).(*uint8)
							sum += float32(*col) * ker
						}
					}
					sum /= float32(sobel_kernel.k)

					xx, yy = replicate_border(x, y, deriv.Bounds())
					deriv.Set(xx, yy, float64(sum))
				}
			}
			done <- 1
		}(image.Rect(b.Min.X, b.Min.Y+i*(b.Dy())/n, b.Max.X, b.Min.Y+(i+1)*(b.Dy())/n))
	}

	for i := 0; i < n; i++ {
		<-done
	}

	return
}
