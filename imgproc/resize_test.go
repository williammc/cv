// Copyright 2014 The Authors. All rights reserved.

package imgproc

import (
	"bitbucket.org/williammccohen/cv/util"
	"image"
	"image/jpeg"
	"log"
	"os"
	"runtime"
	"testing"
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

// Test gradient calculation.
func TestResize(t *testing.T) {
	gray_img := load_data()
	resized_img := Resize(100, 100, gray_img, NearestNeighbor)
	file_path := "/Users/thanhnguyen/dev/goar/trunk/data/family_resized_100x100.jpg"
	util.SaveToFile(file_path, resized_img)
}

// Benchmark for gradient calculation on 640x480 grayscale image.
func BenchmarkResize640x480(b *testing.B) {
	b.StopTimer()
	gray_img := load_data()
	b.StartTimer()
	_ = Resize(100, 100, gray_img, NearestNeighbor)
}

func load_data() *image.Gray {
	// open image
	file, err1 := os.Open("/Users/thanhnguyen/dev/goar/trunk/data/family.jpg")
	if err1 != nil {
		log.Fatal(err1)
	}

	// decode jpeg into image.Image
	img, err2 := jpeg.Decode(file)
	if err2 != nil {
		log.Fatal(err2)
	}
	file.Close()
	return ConvertImageToGrayscale(img)
}
