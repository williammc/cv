// Copyright 2014 The Authors. All rights reserved.

package imgproc

import (
	"image"
)

// Square
func sq(p uint32) uint64 {
	return uint64(p) * uint64(p)
}

// Calculate image integrals (sum & sum squared)
func Integral(img *image.Gray) (*Uint32Image, *Uint64Image) {
	sum_img := NewUint32Image(img.Rect)
	sum_sq_img := NewUint64Image(img.Rect)
	// Do the first row.
	b := img.Bounds()
	var c uint32
	for x := b.Min.X + 1; x < b.Max.X; x++ {
		c = uint32(img.Pix[img.PixOffset(x, 0)])
		sum_img.Set(x, 0, sum_img.Pix[sum_img.PixOffset(x-1, 0)]+c)
		sum_sq_img.Set(x, 0, sum_sq_img.Pix[sum_sq_img.PixOffset(x-1, 0)]+sq(c))
	}

	// Do the first column.
	for y := b.Min.Y + 1; y < b.Max.Y; y++ {
		c = uint32(img.Pix[img.PixOffset(0, y)])
		sum_img.Set(0, y, sum_img.Pix[sum_img.PixOffset(0, y-1)]+c)
		sum_sq_img.Set(0, y, sum_sq_img.Pix[sum_sq_img.PixOffset(0, y-1)]+sq(c))
	}

	// Do the remainder of the image
	var sumline uint32 = 0
	var sumsqline uint64 = 0
	for y := b.Min.Y + 1; y < b.Max.Y; y++ {
		sumline = uint32(img.Pix[img.PixOffset(0, y)])
		sumsqline = sq(sumline)
		for x := b.Min.X + 1; x < b.Max.X; x++ {
			c = uint32(img.Pix[img.PixOffset(x, y)])
			sumline += c
			sumsqline += sq(c)
			sum_img.Set(x, y, sumline+sum_img.Pix[sum_img.PixOffset(x, y-1)])
			sum_sq_img.Set(x, y, sumsqline+sum_sq_img.Pix[sum_sq_img.PixOffset(x, y-1)])
		}
	}

	return sum_img, sum_sq_img
}

func IntegralInPlace(img *image.Gray, sum_img *Uint32Image, sum_sq_img *Uint64Image) {
	// Do the first row.
	b := img.Bounds()
	var c uint32
	for x := b.Min.X + 1; x < b.Max.X; x++ {
		c = uint32(img.Pix[img.PixOffset(x, 0)])
		sum_img.Set(x, 0, sum_img.Pix[sum_img.PixOffset(x-1, 0)]+c)
		sum_sq_img.Set(x, 0, sum_sq_img.Pix[sum_sq_img.PixOffset(x-1, 0)]+sq(c))
	}

	// Do the first column.
	for y := b.Min.Y + 1; y < b.Max.Y; y++ {
		c = uint32(img.Pix[img.PixOffset(0, y)])
		sum_img.Set(0, y, sum_img.Pix[sum_img.PixOffset(0, y-1)]+c)
		sum_sq_img.Set(0, y, sum_sq_img.Pix[sum_sq_img.PixOffset(0, y-1)]+sq(c))
	}

	// Do the remainder of the image
	var sumline uint32 = 0
	var sumsqline uint64 = 0
	for y := b.Min.Y + 1; y < b.Max.Y; y++ {
		sumline = uint32(img.Pix[img.PixOffset(0, y)])
		sumsqline = sq(sumline)
		for x := b.Min.X + 1; x < b.Max.X; x++ {
			c = uint32(img.Pix[img.PixOffset(x, y)])
			sumline += c
			sumsqline += sq(c)
			sum_img.Set(x, y, sumline+sum_img.Pix[sum_img.PixOffset(x, y-1)])
			sum_sq_img.Set(x, y, sumsqline+sum_sq_img.Pix[sum_sq_img.PixOffset(x, y-1)])
		}
	}
}

// Calculate integral of a sub region.
func CalcRect(integral ValueImage, from image.Point, to image.Point) float64 {
	return integral.At(to.X, to.Y) + integral.At(from.X, from.Y) - integral.At(from.Y, to.X) - integral.At(to.Y, from.X)
}
