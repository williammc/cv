// Copyright 2014 The Authors. All rights reserved.

package imgproc

import (
	"image"
	"image/color"
	"runtime"
	"testing"
)

var img = image.NewGray(image.Rect(0, 0, 640, 480))

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	img.Set(1, 1, color.White)
}

func Test_Integral(t *testing.T) {
	b := img.Rect
	for y := b.Min.Y; y < b.Max.Y; y++ {
		for x := b.Min.X; x < b.Max.X; x++ {
			c := color.Gray{1}
			img.Set(x, y, c)
		}
	}

	sum, sum_sq := Integral(img)
	if sum.At(0, 2) != 2 || sum_sq.At(0, 2) != 2 {
		t.Fail()
	}
}

// Benchmark for a transformation of 640x480 grayscale image
func BenchmarkIntegral640x480(b *testing.B) {
	_, _ = Integral(img)
}
