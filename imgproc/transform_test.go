// Copyright 2014 The Authors. All rights reserved.

package imgproc

import (
	"fmt"
	matrix "github.com/skelterjohn/go.matrix"
	"image"
	"image/color"
	"image/jpeg"
	"log"
	"os"
	"runtime"
	"testing"
)

var in_img = image.NewGray(image.Rect(0, 0, 640, 480))
var out_img = image.NewGray(image.Rect(0, 0, 640, 480))
var M = matrix.Eye(2)
var orig = matrix.Scaled(matrix.Ones(2, 1), 30/2)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	img.Set(1, 1, color.White)
}

func Test_Transform(t *testing.T) {
	in_img_access := NewGrayAccessor(in_img)
	out_img_access := NewGrayAccessor(out_img)
	_ = Transform(in_img_access, out_img_access, M, orig, orig)
	if in_img.At(2, 2) != out_img.At(2, 2) {
		t.Fail()
	}
}

// Benchmark for a transformation of 640x480 grayscale image
func BenchmarkTransform640x480(b *testing.B) {
	b.StopTimer()
	in_img_access := NewGrayAccessor(in_img)
	out_img_access := NewGrayAccessor(out_img)
	b.StartTimer()
	_ = Transform(in_img_access, out_img_access, M, orig, orig)
}

// Identity transofmation
func ExampleIdentityTransform(t *testing.T) {
	b := in_img.Rect
	for y := b.Min.Y; y < b.Max.Y; y++ {
		for x := b.Min.X; x < b.Max.X; x++ {
			c := color.Gray{1}
			in_img.Set(x, y, c)
		}
	}
	in_img_access := NewGrayAccessor(in_img)
	out_img_access := NewGrayAccessor(out_img)
	v := Transform(in_img_access, out_img_access, M, orig, orig)
	fmt.Println("Outside pixels:" + string(v))

	out, err := os.Create("transoform.jpg")
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()

	// write new image to file
	jpeg.Encode(out, out_img, nil)

}
