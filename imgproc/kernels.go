// Copyright 2014 The Authors. All rights reserved.

package imgproc

import (
	"image"
	"log"
	"math"
)

type Kernel interface {
	Bounds() image.Rectangle     // Kernel bounding
	Eval(dx, dy float32) float32 // Evaluate kernel value at given point.
}

// =============================== NearestNeighbor ================================
type NearestNeighborKernel struct {
	Rect image.Rectangle
}

func NewNearestNeighborKernel() *NearestNeighborKernel {
	return &NearestNeighborKernel{image.Rect(0, 0, 1, 1)}
}

func (nn *NearestNeighborKernel) Bounds() image.Rectangle {
	return nn.Rect
}

func (nn *NearestNeighborKernel) Eval(dx, dy float32) float32 {
	if math.Abs(float64(dx)) <= 0.5 && math.Abs(float64(dy)) <= 0.5 {
		return 1.0
	}
	return 0.0

}

// =============================== Bilinear ================================
type BilinearKernel struct {
	Rect image.Rectangle
}

func NewBilinearKernel() *BilinearKernel {
	return &BilinearKernel{image.Rect(0, 0, 1, 1)}
}

func (bl *BilinearKernel) Bounds() image.Rectangle {
	return bl.Rect
}

func (bl *BilinearKernel) Eval(dx, dy float32) float32 {
	f := func(x float32) (y float32) {
		absX := float32(math.Abs(float64(x)))
		if absX <= 1 {
			y = 1 - absX
		} else {
			y = 0
		}
		return
	}
	return f(dx) * f(dy)
}

// =============================== BiCubic ================================
type BiCubicKernel struct {
	Rect image.Rectangle
}

func NewBiCubicKernel() *BiCubicKernel {
	return &BiCubicKernel{image.Rect(-1, -1, 1, 1)}
}

func (bc *BiCubicKernel) Bounds() image.Rectangle {
	return bc.Rect
}

func (bc *BiCubicKernel) Eval(dx, dy float32) float32 {
	f := func(x float32) (y float32) {
		absX := float32(math.Abs(float64(x)))
		if absX <= 1 {
			y = absX*absX*(1.5*absX-2.5) + 1
		} else if absX <= 2 {
			y = absX*(absX*(2.5-0.5*absX)-4) + 2
		} else {
			y = 0
		}
		return
	}
	return f(dx) * f(dy)
}

// =============================== SobelXKernel ================================
var sobel_x_kernel = []float32{-1, 0, 1, -1, 0, 1, -1, 0, 1}
var sobel_xx_kernel = []float32{-2, -1, 0, 1, 2,
	-2, -1, 0, 1, 2,
	-2, -1, 0, 1, 2,
	-2, -1, 0, 1, 2,
	-2, -1, 0, 1, 2}

var sobel_y_kernel = []float32{-1, -1, -1, 0, 0, 0, 1, 1, 1}
var sobel_yy_kernel = []float32{-2, -2, -2, -2, -2,
	-1, -1, -1, -1, -1,
	0, 0, 0, 0, 0,
	1, 1, 1, 1, 1,
	2, 2, 2, 2, 2}

var sobel_xy_kernel = []float32{-2, -1, 2, -1, 0, 1, -2, 1, 2}

type SobelKernel struct {
	Rect   image.Rectangle
	dx, dy uint
	k      int
	data   []float32 // Kernel matrix data.
}

func NewSobelKernel(dx, dy uint) *SobelKernel {
	sb := new(SobelKernel)
	sb.dx = dx
	sb.dy = dy
	switch dx + dy {
	case 1:
		if dx == 1 {
			sb.data = sobel_x_kernel
		} else {
			sb.data = sobel_y_kernel
		}
		sb.k = 3
		sb.Rect = image.Rect(-1, -1, 1, 1)
		break
	case 2:
		if dx == 2 {
			sb.data = sobel_xx_kernel
		} else if dy == 2 {
			sb.data = sobel_yy_kernel
		} else {
			sb.data = sobel_xy_kernel
		}
		sb.k = 5
		sb.Rect = image.Rect(-2, -2, 2, 2)
		break
	default:
		log.Fatal("Wrong dx, dy!")
		return nil
	}
	return sb
}

func (sb *SobelKernel) Bounds() image.Rectangle {
	return sb.Rect
}

func (sb *SobelKernel) Eval(dx, dy float32) float32 {
	xf := int(math.Floor(float64(dx)))
	yf := int(math.Floor(float64(dy)))

	return sb.data[(yf+sb.k/2)*sb.k+(xf+sb.k/2)]
}
