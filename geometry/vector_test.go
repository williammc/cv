// Copyright 2014 The Authors. All rights reserved.

package geometry

import (
	"math"
	"runtime"
	"testing"
)

var v = MakeVector(10)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	v.Set(0, float64(2.0))
	v.Set(9, float64(2.0))
}

// Length of normalized vector has to be one.
func TestNormalize(t *testing.T) {
	another_v := v.Normalize()
	sum_sq := float64(0.0)
	for i := 0; i < another_v.Rows(); i++ {
		sum_sq += another_v.Get(i) * another_v.Get(i)
	}

	if math.Abs(sum_sq-1.0) > 1.e-9 {
		t.Fail()
	}
}

// Length of normalized vector has to be one.
func TestNormalized(t *testing.T) {
	v.Normalized()
	sum_sq := float64(0.0)
	for i := 0; i < v.Rows(); i++ {
		sum_sq += v.Get(i) * v.Get(i)
	}

	if math.Abs(sum_sq-1.0) > 1.e-9 {
		t.Fail()
	}
}

// Length of normalized vector has to be one.
func TestVector2dNormalized(t *testing.T) {
	var v_2d = MakeVector2d(2, 1)
	v_2d.Normalized()
	sum_sq := float64(0.0)
	for i := 0; i < v_2d.Rows(); i++ {
		sum_sq += v_2d.Get(i) * v_2d.Get(i)
	}

	if math.Abs(sum_sq-1.0) > 1.e-9 {
		t.Fail()
	}
}

// Length of normalized vector has to be one.
func TestVector3dNormalized(t *testing.T) {
	var v_3d = MakeVector3d(3, 1, 1)
	v_3d.Normalized()
	sum_sq := float64(0.0)
	for i := 0; i < v_3d.Rows(); i++ {
		sum_sq += v_3d.Get(i) * v_3d.Get(i)
	}

	if math.Abs(sum_sq-1.0) > 1.e-9 {
		t.Fail()
	}
}

// Length of normalized vector has to be one.
func TestVector4dNormalized(t *testing.T) {
	var v_4d = MakeVector4d(3, 1, 1, 2)
	v_4d.Normalized()
	sum_sq := float64(0.0)
	for i := 0; i < v_4d.Rows(); i++ {
		sum_sq += v_4d.Get(i) * v_4d.Get(i)
	}

	if math.Abs(sum_sq-1.0) > 1.e-9 {
		t.Fail()
	}
}
