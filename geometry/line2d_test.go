// Copyright 2014 The Authors. All rights reserved.

package geometry

import (
	"runtime"
	"testing"
)

var p1 = MakeVector2d(0, 0)
var p2 = MakeVector2d(1, 1)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func TestPoint2dConstruction(t *testing.T) {
	_ = MakeLine2d(*p1, *p2)
}
