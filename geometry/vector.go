// Copyright 2014 The Authors. All rights reserved.

package geometry

import (
	matrix "github.com/skelterjohn/go.matrix"
	"math"
)

type Vector struct {
	matrix.DenseMatrix
}

func MakeVector(s int) *Vector {
	v := Vector{*matrix.Zeros(s, 1)}
	return &v
}

func (v *Vector) Length() int {
	return v.Rows()
}

func (v *Vector) Get(i int) float64 {
	return v.DenseMatrix.Get(i, 0)
}

func (v *Vector) Set(i int, value float64) {
	v.DenseMatrix.Set(i, 0, value)
}

// Slice from Vector and return reference object to the internal data.
func (v *Vector) Slice(i, n int) (res *Vector) {
	t := v.DenseMatrix.Array()
	t = t[i : i+n-1]
	res = MakeVector(n)
	m := matrix.MakeDenseMatrix(t, n, 1)
	res.DenseMatrix.SetMatrix(0, 0, m)
	return
}

func (v *Vector) Normalized() *Vector {
	sum_sq := float64(0.0)
	for i := 0; i < v.Rows(); i++ {
		sum_sq += v.Get(i) * v.Get(i)
	}
	sum_sq = math.Sqrt(sum_sq)

	for i := 0; i < v.Rows(); i++ {
		v.Set(i, v.Get(i)/sum_sq)
	}

	return v
}

func (v *Vector) Normalize() (another_v *Vector) {
	another_v = MakeVector(v.Rows())
	sum_sq := float64(0.0)
	for i := 0; i < v.Rows(); i++ {
		sum_sq += v.Get(i) * v.Get(i)
	}
	sum_sq = math.Sqrt(sum_sq)

	for i := 0; i < v.Rows(); i++ {
		another_v.Set(i, v.Get(i)/sum_sq)
	}
	return
}

func (v *Vector) Norm() float64 {
	sum_sq := float64(0.0)
	for i := 0; i < v.Rows(); i++ {
		sum_sq += v.Get(i) * v.Get(i)
	}
	return math.Sqrt(sum_sq)
}

func (v *Vector) SquaredNorm() float64 {
	sum_sq := float64(0.0)
	for i := 0; i < v.Rows(); i++ {
		sum_sq += v.Get(i) * v.Get(i)
	}
	return sum_sq
}

func (v *Vector) InfNorm() float64 {
	n := math.Abs(v.Get(0))
	for i := 1; i < v.Rows(); i++ {
		n = math.Max(n, math.Abs(v.Get(i)))
	}
	return n
}

// 2D Vector
type Vector2d struct {
	Vector
}

func MakeVector2d(x, y float64) *Vector2d {
	m := matrix.MakeDenseMatrix([]float64{x, y}, 2, 1)
	p := Vector2d{Vector{*m}}
	return &p
}

// 3D Vector
type Vector3d struct {
	Vector
}

func MakeVector3d(x, y, z float64) *Vector3d {
	m := matrix.MakeDenseMatrix([]float64{x, y, z}, 3, 1)
	p := Vector3d{Vector{*m}}
	return &p
}

func (v *Vector) Cross(another_v *Vector) (res *Vector3d) {
	res = MakeVector3d(0, 0, 0)
	res.Set(0, v.Get(1)*another_v.Get(2)-v.Get(2)*another_v.Get(1))
	res.Set(1, v.Get(2)*another_v.Get(0)-v.Get(0)*another_v.Get(2))
	res.Set(2, v.Get(0)*another_v.Get(1)-v.Get(1)*another_v.Get(0))
	return
}

// 4D Vector (homogenous)
type Vector4d struct {
	Vector
}

func MakeVector4d(x, y, z, w float64) *Vector4d {
	m := matrix.MakeDenseMatrix([]float64{x, y, z, w}, 4, 1)
	p := Vector4d{Vector{*m}}
	return &p
}

func Project(v *Vector) *Vector {
	len_1 := v.Rows() - 1
	vr := MakeVector(len_1)
	for i := 0; i < len_1; i++ {
		vr.Set(i, v.Get(i)/v.Get(len_1))
	}
	return vr
}

func UnProject(v *Vector) *Vector {
	len_1 := v.Rows() + 1
	vr := MakeVector(len_1)
	for i := 0; i < len_1-1; i++ {
		vr.Set(i, v.Get(i))
	}
	vr.Set(len_1-1, 1)
	return vr
}
