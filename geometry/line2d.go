// Copyright 2014 The Authors. All rights reserved.

package geometry

import ()

// Represent a 2D line.
type Line2d struct {
	point1      Vector2d
	point2      Vector2d
	is_bounded  bool
	a, b, c     float64
	line_vector Vector2d
}

func MakeLine2d(p1, p2 Vector2d) *Line2d {
	line := new(Line2d)
	line.point1 = p1
	line.point2 = p2
	temp, _ := p2.MinusDense(&p1.DenseMatrix)
	line.line_vector = Vector2d{Vector{*temp}}
	line.line_vector.Normalized()
	return line
}
