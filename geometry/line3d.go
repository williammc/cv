// Copyright 2014 The Authors. All rights reserved.

package geometry

import (
	matrix "github.com/skelterjohn/go.matrix"
)

// Represent a 3D line.
type Line3d struct {
	point1      Vector3d
	point2      Vector3d
	line_vector Vector3d
}

func MakeLine3d(p1, p2 Vector3d) *Line3d {
	line := new(Line3d)
	line.point1 = p1
	line.point2 = p2
	temp, _ := p2.MinusDense(&p1.DenseMatrix)
	line.line_vector = Vector3d{Vector{*temp}}
	line.line_vector.Normalized()
	return line
}

// projects a 3D point onto the line.
func (line *Line3d) ProjectPoint(p *Vector3d) *Vector3d {
	temp1, _ := p.MinusDense(&line.point1.DenseMatrix)
	s, _ := temp1.Transpose().TimesDense(&line.line_vector.DenseMatrix)
	temp := matrix.Scaled(&line.line_vector.DenseMatrix, s.Get(0, 0))
	pr, _ := line.point1.PlusDense(temp)
	return &Vector3d{Vector{*pr}}
}
