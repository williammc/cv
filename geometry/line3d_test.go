// Copyright 2014 The Authors. All rights reserved.

package geometry

import (
	"runtime"
	"testing"
)

var p31 = MakeVector3d(0, 0, 0)
var p32 = MakeVector3d(1, 1, 0)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func TestPoint3dConstruction(t *testing.T) {
	_ = MakeLine3d(*p31, *p32)
}
